import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;
import pl.academy.sda.repository.csv.writers.PaperBookToCSVWriter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PaperBookFactory {

    static int id = 1;

    static PaperBook randomPaperBook(){
        Random random = new Random();
        return new PaperBook(
                id++,
                "RANDOM_PAPER_BOOK" + random.nextInt(10),
                new BigDecimal(random.nextDouble()*100),
                "Tolkien",
                (int)(random.nextDouble()*1000)+200
        );
    }

    public static void main(String[] args) {
        List<Product> paperBooks = new ArrayList<>();
        for(int i=0; i<100; i++){
            paperBooks.add(randomPaperBook());
        }
        PaperBookToCSVWriter paperBookToCSVWriter = new PaperBookToCSVWriter("paperBooks");
        paperBookToCSVWriter.saveProducts(paperBooks);
    }

}
