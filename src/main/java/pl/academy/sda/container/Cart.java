package pl.academy.sda.container;

import pl.academy.sda.dto.products.Product;

import java.math.BigDecimal;
import java.util.*;

public class Cart {

    final List<Product> productsInCart;

    public Cart() {
        productsInCart = new LinkedList<>();
    }

    public Cart(List<Product> productsInCart) {
        this.productsInCart = productsInCart;
    }

    public void addProductToCart(Product product) {
        productsInCart.add(product);
    }

    public void removeFromCart(Product product) {
        productsInCart.remove(product);
    }

    public void removeProductWithIndex(int index) {
        productsInCart.remove(index);
    }

    public List<Product> getProductsInCart() {
        return productsInCart;
    }
}
