package pl.academy.sda.container;

import pl.academy.sda.dto.products.Product;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ReceiptLine {
    private final long id;
    private final String name;
    private final BigDecimal price;
    private final BigDecimal priceAfterDiscount;

    private static final String LINE_FORMAT = "%s\t|%s\t|%s\t|%s";
    private String currency;


    public ReceiptLine(Product product, BigDecimal priceAfterDiscount, BigDecimal ratio, String currency) {
        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice().divide(ratio, 2, BigDecimal.ROUND_HALF_UP);
        this.priceAfterDiscount = priceAfterDiscount.divide(ratio, 2, BigDecimal.ROUND_HALF_UP);
        this.currency = currency;
    }

    public static String getHeader(){
        return String.format(LINE_FORMAT, "id", "name", "price", "price after discount");
    }

    @Override
    public String toString(){
        return String.format(LINE_FORMAT, id, name, price.setScale(2, RoundingMode.HALF_UP), priceAfterDiscount.setScale(2, RoundingMode.HALF_UP), currency);
    }
}
