package pl.academy.sda.service;

import pl.academy.sda.container.Cart;
import pl.academy.sda.container.ReceiptGenerator;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.repository.DatabaseFacade;
import pl.academy.sda.repository.csv.CSVDataBase;

import java.io.IOException;
import java.util.List;

public class ProductService {

    final DatabaseFacade database;

    public ProductService(String pathToDatabase) throws IOException {
        database = new CSVDataBase(pathToDatabase);
    }

    public ProductService() throws  IOException {
        this("src/main/resources/csv/defaultDatabase");
    }

    public List<Product> getAllProductsList() {
        return database.getAllProducts();
    }

    public List<String> finalizeTransactionAndReturnRecipe(Cart cart){
        deleteSelledProduct(cart);
        return new ReceiptGenerator(cart).createRecipe();
    }

    private void deleteSelledProduct(Cart cart) {
        cart.getProductsInCart().stream().forEach(database::removeProduct);
    }

}
