package pl.academy.sda;

import pl.academy.sda.container.Cart;
import pl.academy.sda.container.ReceiptGenerator;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.audio.AudioAlbum;
import pl.academy.sda.dto.products.printed.Poster;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class Main2 {

    public static void main(String args[]) {
        ThreeTypeStorage<Poster, Double, String> threeTypeStorage = new ThreeTypeStorage<>(new Poster(0, "", BigDecimal.ONE), 0.0, "");
        System.out.println(threeTypeStorage.getType1ClassName());
        System.out.println(threeTypeStorage.getType2ClassName());
        System.out.println(threeTypeStorage.getType3ClassName());

        ThreeTypeStorage threeTypeStorage2 = new ThreeTypeStorage("", "", "");
        System.out.println(threeTypeStorage2.getType1ClassName());
        System.out.println(threeTypeStorage2.getType2ClassName());
        System.out.println(threeTypeStorage2.getType3ClassName());
    }

}

class ThreeTypeStorage<TYPE_1, TYPE_2, TYPE_3> {
    TYPE_1 object1;
    TYPE_2 object2;
    TYPE_3 object3;

    //cannot do TYPE_1.class or sth

    public ThreeTypeStorage(TYPE_1 object1, TYPE_2 object2, TYPE_3 object3) {
        this.object1 = object1;
        this.object2 = object2;
        this.object3 = object3;
    }

    public String getType1ClassName(){
        return object1.getClass().getName();
    }

    public String getType2ClassName(){
        return object2.getClass().getName();
    }

    public String getType3ClassName(){
        return object3.getClass().getName();
    }
}
