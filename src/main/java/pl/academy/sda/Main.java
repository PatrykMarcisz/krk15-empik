package pl.academy.sda;

import pl.academy.sda.container.Cart;
import pl.academy.sda.container.ReceiptGenerator;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.audio.AudioAlbum;
import pl.academy.sda.service.ProductService;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static void main(String args[]) throws IOException {
        ProductService service = new ProductService();

        List<Product> allProductsList = service.getAllProductsList();

        List<Product> toBuy = IntStream.range(0, allProductsList.size())
                .filter(n -> n % 3 == 0)
                .mapToObj(allProductsList::get)
                .collect(Collectors.toList());

        Cart cart = new Cart(toBuy);
        List<String> strings = service.finalizeTransactionAndReturnRecipe(cart);
        strings.forEach(System.out::println);
    }
}
