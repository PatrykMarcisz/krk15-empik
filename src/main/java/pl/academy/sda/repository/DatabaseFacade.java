package pl.academy.sda.repository;

import pl.academy.sda.dto.products.Product;

import java.util.Collection;
import java.util.List;

//to tzw CRUD - create, read, update, delete
public interface DatabaseFacade {

    Product getProduct(long id);

    List<Product> getAllProducts();

    void addProduct(Product product);

    void addManyProducts(Collection<Product> products);

    void update(Product updatedProduct);

    void removeProduct(Product product);

    //TODO implement, maybe some optimisation ?
    //int productsCounter()
}
