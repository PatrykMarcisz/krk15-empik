package pl.academy.sda.repository.api.nbp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SingleRate {

    private String currency;

    private String code;

    private Double currencySellCourse;

    private Double currencyBuyCourse;

    @JsonCreator
    public SingleRate(
            @JsonProperty("currency") String currency,
            @JsonProperty("code") String code,
            @JsonProperty("bid") Double currencySellCourse,
            @JsonProperty("ask") Double currencyBuyCourse) {
        this.currency = currency;
        this.code = code;
        this.currencySellCourse = currencySellCourse;
        this.currencyBuyCourse = currencyBuyCourse;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public Double getCurrencySellCourse() {
        return currencySellCourse;
    }

    public Double getCurrencyBuyCourse() {
        return currencyBuyCourse;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCurrencySellCourse(Double currencySellCourse) {
        this.currencySellCourse = currencySellCourse;
    }

    public void setCurrencyBuyCourse(Double currencyBuyCourse) {
        this.currencyBuyCourse = currencyBuyCourse;
    }

    @Override
    public String toString() {
        return "SingleRate{" +
                "currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", currencySellCourse=" + currencySellCourse +
                ", currencyBuyCourse=" + currencyBuyCourse +
                '}';
    }
}

