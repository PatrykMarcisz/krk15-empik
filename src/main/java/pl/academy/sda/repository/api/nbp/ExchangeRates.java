package pl.academy.sda.repository.api.nbp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ExchangeRates {

    private final String tableNumber;

    private String tableType;

    private String tradingDate;

    private String effectiveDate;

    private List<SingleRate> rates;

    @JsonCreator
    public ExchangeRates(
            @JsonProperty(value = "table") String tableType,
            @JsonProperty(value = "no") String tableNumber,
            @JsonProperty(value = "tradingDate") String tradingDate,
            @JsonProperty(value = "effectiveDate") String effectiveDate,
            @JsonProperty(value = "rates") List<SingleRate> rates) {
        this.tableType = tableType;
        this.tableNumber = tableNumber;
        this.tradingDate = tradingDate;
        this.effectiveDate = effectiveDate;
        this.rates = rates;
    }

    public String getTableType() {
        return tableType;
    }

    public String getTradingDate() {
        return tradingDate;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public List<SingleRate> getRates() {
        return rates;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public void setTradingDate(String tradingDate) {
        this.tradingDate = tradingDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public void setRates(List<SingleRate> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "ExchangeRates{" +
                "tableNumber='" + tableNumber + '\'' +
                ", tableType='" + tableType + '\'' +
                ", tradingDate='" + tradingDate + '\'' +
                ", effectiveDate='" + effectiveDate + '\'' +
                ", rates=" + rates +
                '}';
    }
}
