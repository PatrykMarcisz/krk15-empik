package pl.academy.sda.repository.api.nbp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

public class NBPRepository {
    private final static String BASE_REQUEST_URL = "http://api.nbp.pl/api/exchangerates/tables/C";
    
    private final HttpClient httpClient;

    public NBPRepository() {
        httpClient = HttpClients.createDefault() ;
    }

    public List<ExchangeRates> getResponse(){
        HttpGet httpGet = new HttpGet(BASE_REQUEST_URL);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            InputStream contentOfResponse = response.getEntity().getContent();
            Scanner s = new Scanner(contentOfResponse).useDelimiter("\\A");
            String jsonAsString = s.hasNext() ? s.next() : "";
            ObjectMapper objectMapper = new ObjectMapper();
            List<ExchangeRates> exchangeRatesList = objectMapper.readValue(
                    jsonAsString,
                    new TypeReference<List<ExchangeRates>>() {
                    });
            return exchangeRatesList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new ParseException("cannot parse response");
    }
}
