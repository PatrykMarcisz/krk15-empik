package pl.academy.sda.repository;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;
import pl.academy.sda.repository.csv.entities.CSVPaperBook;

import java.io.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

//robiony w trakcie zajec
public class DatabaseFacadeCsv implements DatabaseFacade {
    @Override
    public Product getProduct(long id) {
        return null;
    }

    @Override
    public List<Product> getAllProducts() {
        try {
            CsvToBean csvToBeanConverter = new CsvToBeanBuilder(
                    new FileReader("src/main/resources/paperbooks-read.csv"))
                    .withType(CSVPaperBook.class)
                    .build();

            List<CSVPaperBook> beans = csvToBeanConverter.parse();
            return beans.stream()
                    .map(x -> x.toProduct())
                    .peek(x -> System.out.println(x.getName() + " " + x.getPrice()))
                    .collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    @Override
    public void addProduct(Product product) {
        product = new PaperBook(2, "name", new BigDecimal(10), "author", 400);
        try {
            Writer writer = new FileWriter("src/main/resources/paperbooks-save.csv");
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            beanToCsv.write(product);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        } catch (CsvDataTypeMismatchException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addManyProducts(Collection<Product> products) {
        for(Product prod : products){
            addProduct(prod);
        }
    }

    @Override
    public void update(Product updatedProduct) {

    }

    @Override
    public void removeProduct(Product product) {

    }
}
