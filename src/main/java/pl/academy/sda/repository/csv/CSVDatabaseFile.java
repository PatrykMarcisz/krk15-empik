package pl.academy.sda.repository.csv;

import java.io.File;
import java.io.IOException;

public interface CSVDatabaseFile {

    default File openOrCreateFile(String databaseFilePath) {
        File file = new File(databaseFilePath);
        File parent = file.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }
        if (!file.exists()) {
            try {
                boolean newFile = file.createNewFile();
                System.out.println(newFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
