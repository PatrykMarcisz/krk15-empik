package pl.academy.sda.repository.csv.writers;

import pl.academy.sda.dto.products.Product;
import pl.academy.sda.repository.csv.CSVDatabaseFile;

import java.util.Collection;

public interface ProductToCSVWriter extends CSVDatabaseFile {

    void saveProducts(Collection<Product> products, String pathToDatabase);

}
