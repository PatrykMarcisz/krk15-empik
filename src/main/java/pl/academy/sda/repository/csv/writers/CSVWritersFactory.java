package pl.academy.sda.repository.csv.writers;

import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CSVWritersFactory {

    private static Map<Class, ProductToCSVWriter> writers;

    static {
        writers = new HashMap<>();
        writers.put(PaperBook.class, new PaperBookToCSVWriter());
    }

    public static ProductToCSVWriter baseOnProduct(Product product){
        if(writers.containsKey(product.getClass())){
            return writers.get(product.getClass());
        }
        throw new IllegalArgumentException("no suitable ProductToCSVWriter for class " + product.getClass().getName());
    }
}
