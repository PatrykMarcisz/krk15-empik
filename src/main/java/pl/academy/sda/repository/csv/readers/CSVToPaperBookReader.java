package pl.academy.sda.repository.csv.readers;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.repository.csv.entities.CSVPaperBook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.stream.Collectors;

public class CSVToPaperBookReader implements CSVToProductReader {

    private static final String FILE_NAME = "paperbooks";

    public Collection<Product> getProducts(String databasePath) {
        try {
            //this.openOrCreateFile is ok when only implements one interface with that method
            File file = CSVToProductReader.super.openOrCreateFile(databasePath.concat(File.separator).concat(FILE_NAME).concat(".csv"));
            System.out.println(file.getAbsolutePath());

            CsvToBean<CSVPaperBook> csvToBean = new CsvToBeanBuilder<CSVPaperBook>(new FileReader(file))
                    .withType(CSVPaperBook.class)
                    .build();

            List<Product> results = csvToBean.parse().stream()
                    .map(CSVPaperBook::toProduct)
                    .collect(Collectors.toList());

            return results;

        } catch (FileNotFoundException | RuntimeException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }



}
