package pl.academy.sda.repository.csv.entities;

import com.opencsv.bean.CsvBindByName;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;

import java.math.BigDecimal;

public class CSVPaperBook implements CSVProductEntity {

    @CsvBindByName(column = "id", required = true)
    private Long id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "price", required = true)
    private String price;

    @CsvBindByName(column = "author", required = true)
    private String author;

    @CsvBindByName(column = "pageCounter", required = true)
    private Integer pageCounter;

    public Product toProduct(){
        return new PaperBook(id, name, new BigDecimal(price), author, pageCounter);
    }
}
