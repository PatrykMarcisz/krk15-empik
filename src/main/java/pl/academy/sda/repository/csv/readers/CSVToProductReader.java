package pl.academy.sda.repository.csv.readers;


import pl.academy.sda.dto.products.Product;
import pl.academy.sda.repository.csv.CSVDatabaseFile;

import java.util.Collection;

public interface CSVToProductReader extends CSVDatabaseFile {

    Collection<Product> getProducts(String dataBase);


}
