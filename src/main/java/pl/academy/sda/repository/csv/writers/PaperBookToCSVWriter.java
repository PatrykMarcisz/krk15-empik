package pl.academy.sda.repository.csv.writers;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class PaperBookToCSVWriter implements ProductToCSVWriter {

    final static String FILE_NAME = "paperbooks";

    public PaperBookToCSVWriter() {
    }

    @Override
    public void saveProducts(Collection<Product> products, String databaseFileName) {
        try {
            File file = ProductToCSVWriter.super.openOrCreateFile(databaseFileName.concat(File.separator).concat(FILE_NAME).concat(".csv"));
            Writer writer = new FileWriter(file);
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            List<PaperBook> beansToSave = products.stream().map(x -> (PaperBook) x).collect(Collectors.toList());
            beanToCsv.write(beansToSave);
            writer.close();
        } catch (CsvDataTypeMismatchException | IOException | CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        }
    }
}