package pl.academy.sda.repository.csv.entities;

import pl.academy.sda.dto.products.Product;

public interface CSVProductEntity {

    Product toProduct();
}
