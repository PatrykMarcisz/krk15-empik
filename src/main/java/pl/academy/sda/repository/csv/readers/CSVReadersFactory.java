package pl.academy.sda.repository.csv.readers;

import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CSVReadersFactory {

    private static Map<Class, CSVToProductReader> readers;

    static {
        readers = new HashMap<>();
        readers.put(PaperBook.class, new CSVToPaperBookReader());
    }

    public static CSVToProductReader baseOnProductType(Product product){
        if(readers.containsKey(product.getClass())){
            return readers.get(product.getClass());
        }
        throw new IllegalArgumentException("no suitable CSVReader for class " + product.getClass().getName());
    }

    public static Collection<CSVToProductReader> getAllReaders(){
        return readers.values();
    }
}
