package pl.academy.sda.repository.csv;

import pl.academy.sda.dto.products.Product;
import pl.academy.sda.repository.DatabaseFacade;
import pl.academy.sda.repository.csv.readers.CSVReadersFactory;
import pl.academy.sda.repository.csv.readers.CSVToProductReader;
import pl.academy.sda.repository.csv.writers.CSVWritersFactory;
import pl.academy.sda.repository.csv.writers.ProductToCSVWriter;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class CSVDataBase implements DatabaseFacade {

    private Collection<CSVToProductReader> readers;

    private String databaseBasePath;

    public CSVDataBase(String databaseBasePath) throws IOException {
        this.databaseBasePath = databaseBasePath;
        readers = CSVReadersFactory.getAllReaders();
    }


    @Override
    public Product getProduct(long id) {
        try {
            return getAllProducts().stream()
                    .filter(x -> x.getId() == id)
                    .findFirst()
                    .get();

        } catch (NoSuchElementException e) {
            System.out.println("element not found");
            return null;
        }
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> results = new ArrayList<>();
        for (CSVToProductReader provider : readers) {
            results.addAll(provider.getProducts(databaseBasePath));
        }
        return results;
    }

    @Override
    public void addProduct(Product product) {
        CSVWritersFactory.baseOnProduct(product);

    }

    @Override
    public void addManyProducts(Collection<Product> products) {
        for (Product product : products) {
            addProduct(product);
        }
    }

    @Override
    public void update(Product updatedProduct) {
        Collection<Product> products = getExistingProductsOfType(updatedProduct);
        products.stream().map(x -> x.getId() == updatedProduct.getId() ? updatedProduct : x).collect(Collectors.toList());
        saveProductsOfType(updatedProduct, products);
    }

    @Override
    public void removeProduct(Product productToRemove) {
        Collection<Product> existingProductsOfType = getExistingProductsOfType(productToRemove);
        List<Product> listOfProductsWithoutRemoved = existingProductsOfType
                .stream()
                .filter(product -> productToRemove.getId() != product.getId())
                .collect(Collectors.toList());
        saveProductsOfType(productToRemove, listOfProductsWithoutRemoved);
    }

    private Collection<Product> getExistingProductsOfType(Product product) {
        CSVToProductReader csvToProductReader = CSVReadersFactory.baseOnProductType(product);
        return csvToProductReader.getProducts(databaseBasePath);
    }

    private void saveProductsOfType(Product updatedProduct, Collection<Product> products) {
        ProductToCSVWriter productToCSVWriter = CSVWritersFactory.baseOnProduct(updatedProduct);
        productToCSVWriter.saveProducts(products, databaseBasePath);
    }
}
