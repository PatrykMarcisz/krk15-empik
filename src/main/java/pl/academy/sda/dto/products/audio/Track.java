package pl.academy.sda.dto.products.audio;

public class Track {
    private final String name;
    private final int duration;

    public Track(String name, int duration) {
        this.name = name;
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }
}
