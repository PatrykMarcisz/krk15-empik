package pl.academy.sda.dto.products;

import java.math.BigDecimal;

public class ProductWithDiscount {
    private final Product product;
    private final int discountPercentage;

    public ProductWithDiscount(Product product, int discountPercentage) {
        this.product = product;
        this.discountPercentage = discountPercentage;
    }

    public Product getProduct() {
        return product;
    }

    public BigDecimal getPriceAfterDiscount(){
        return new BigDecimal("10.0");
    }
}
