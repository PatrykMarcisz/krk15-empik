package pl.academy.sda.dto.products.book;

import java.math.BigDecimal;
import java.util.List;

public class Ebook extends Book {

    private final List<String> supportedFormats;

    public Ebook(long id, String name, BigDecimal price, String author, int pageCounter, List<String> supportedFormats) {
        super(id, name, price, author, pageCounter);
        this.supportedFormats = supportedFormats;
    }
}
