package pl.academy.sda.dto.products.audio;

import pl.academy.sda.dto.products.Product;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class AudioAlbum extends Product {
    private final List<Track> tracks;
    private final LocalDate publishmentDate;

    public AudioAlbum(long id, String name, BigDecimal price, List<Track> tracks, LocalDate publishmentDate) {
        super(id, name, price); //this();
        this.tracks = tracks;
        this.publishmentDate = publishmentDate;
        System.out.println(this.toString()); //toString()
    }

    @Override
    public String toString() {
        return String.format("object AudioAlbum created: id: %d\n name: %s\n price: %s\n tracks: %s\n publishmentDate %s",
                getId(),
                getName(),
                getPrice(),
                tracks.stream()
                        .map(track -> String.format("trackName : %s, duration[s]: %d", track.getName(), track.getDuration()))
                        .reduce("", (reducedValue, actualValue) -> reducedValue = reducedValue + "\n" + actualValue),
                publishmentDate
        );
    }

    public AudioAlbum() {
        this(
                1L,
                "default_name",
                new BigDecimal(10),
                Collections.singletonList(new Track("first track", 150)),
                LocalDate.of(1998, 1, 1));
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public LocalDate getPublishmentDate() {
        return publishmentDate;
    }
}
