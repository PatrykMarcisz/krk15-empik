package pl.academy.sda.dto.products.printed;

import pl.academy.sda.dto.products.Product;

import java.math.BigDecimal;

public class Poster extends Product {

    public Poster(long id, String name, BigDecimal price) {
        super(id, name, price);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
