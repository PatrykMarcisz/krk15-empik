package pl.academy.sda;

import pl.academy.sda.dto.products.book.AudioBook;
import pl.academy.sda.dto.products.book.Ebook;
import pl.academy.sda.dto.products.book.PaperBook;

import java.util.*;

class Main3{
    public static void main(String[] args) {
        NewGenerationOfProducts product = new NewGenerationOfProducts(BookCategories.E_BOOK);

        System.out.println(isBook(product));
        System.out.println(isPoster(product));
    }

    static boolean isBook(NewGenerationOfProducts product){
        return Category.BOOKS.is(product.detailedCategory);
    }

    static boolean isPoster(NewGenerationOfProducts products){
        return Category.POSTERS.is(products.detailedCategory);
    }
}

class NewGenerationOfProducts {
    DetailedCategory detailedCategory;

    public NewGenerationOfProducts(DetailedCategory detailedCategory) {
        this.detailedCategory = detailedCategory;
    }
}

enum Category {
    BOOKS,
    POSTERS,
    GAMES;

    static private Map<Category, List<DetailedCategory>> detailCategoryToCategory;

    static {
        detailCategoryToCategory = new HashMap<>();
        List<DetailedCategory> booksSubcategories = Arrays.asList(
                BookCategories.AUDIO_BOOK,
                BookCategories.E_BOOK,
                BookCategories.PAPER_BOOK
        );
        detailCategoryToCategory.put(BOOKS, booksSubcategories);
    }

    boolean is(DetailedCategory detailCategory ){
        Optional<List<DetailedCategory>> optional =
                Optional.ofNullable(detailCategoryToCategory.get(this));

        List<DetailedCategory> detailedCategories = optional.orElse(Collections.emptyList());

        return detailedCategories.contains(detailCategory);
    }
}

interface DetailedCategory {};

enum BookCategories implements DetailedCategory {
    AUDIO_BOOK(AudioBook.class),
    E_BOOK(Ebook.class),
    PAPER_BOOK(PaperBook.class);

    private Class clazz;

    BookCategories(Class clazz) {
        this.clazz = clazz;
    }
}





