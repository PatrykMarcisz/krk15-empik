package pl.academy.sda.repository.api.nbp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class SingleRateTest {

    String jsontoParse = "{\n" +
            "        \"currency\":\"korona szwedzka\",\n" +
            "        \"code\":\"SEK\",\n" +
            "        \"bid\":0.4086,\n" +
            "        \"ask\":0.4168\n" +
            "      }";

    @Test
    void test() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        SingleRate singleRate = mapper.readValue(jsontoParse.getBytes(), SingleRate.class);
        System.out.println(singleRate.getCurrency() + " "
                + singleRate.getCode() + " "
                + singleRate.getCurrencyBuyCourse() + " "
                + singleRate.getCurrencySellCourse());
    }

}