package pl.academy.sda.repository.api.nbp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

class ExchangeRatesParsingTest {

    private File jsonToParse = new File("src/test/resources/nbp.json");

    @Test
    void shouldParseJsonInProperWay() throws IOException {
        System.out.println(jsonToParse.getAbsolutePath());
        ObjectMapper mapper = new ObjectMapper();
        List<ExchangeRates> exchangeRatesList = mapper.readValue(
                        jsonToParse,
                        new TypeReference<List<ExchangeRates>>() {
        });
        System.out.println(exchangeRatesList);
    }

}