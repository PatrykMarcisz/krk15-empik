package pl.academy.sda.repository.csv.writers;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.academy.sda.dto.products.book.PaperBook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PaperBookToCSVWriterTest {

    private final String pathToWriteFile = "src/test/resources/writeTestDatabase/paperbooks.csv";

    private final String fileAsBase64String = "ImF1dGhvciIsImlkIiwibmFtZSIsInBhZ2VDb3VudGVyIiwicHJpY2UiCiJhdXRob3IiLCIxIiwibmFtZSIsIjMwMCIsIjEwIgo=";

    @BeforeEach
    void setUp(){
        File file = new File(pathToWriteFile);
        file.delete();
    }

    @Test
    void shouldWriteToFileProperly() throws IOException {
        PaperBookToCSVWriter writer = new PaperBookToCSVWriter();
        writer.saveProducts(Arrays.asList(new PaperBook(1, "name", new BigDecimal(10), "author", 300)), "src/test/resources/writeTestDatabase");
        File file = new File(pathToWriteFile);
        byte[] encoded = Base64.encodeBase64(fileAsByteArray(file));
        String encodedString = new String(encoded, StandardCharsets.ISO_8859_1);
        assertEquals(fileAsBase64String, encodedString);
    }

    private static byte[] fileAsByteArray(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }

}