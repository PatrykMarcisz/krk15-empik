package pl.academy.sda.repository.csv.readers;

import org.junit.jupiter.api.Test;
import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.book.PaperBook;

import java.math.BigDecimal;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class CSVToPaperBookReaderTest {

    @Test
    void shouldReadFromFile(){
        CSVToPaperBookReader reader = new CSVToPaperBookReader();

        Collection<Product> products = reader.getProducts("src/test/resources/readTestDatabase");
        PaperBook firstElement = (PaperBook)products.stream().findFirst().get();

        assertEquals(1L, firstElement.getId());
        assertEquals(new BigDecimal(10), firstElement.getPrice());
        assertEquals("name", firstElement.getName());
        assertEquals("author", firstElement.getAuthor());
        assertEquals(300, firstElement.getPageCounter());
    }

}