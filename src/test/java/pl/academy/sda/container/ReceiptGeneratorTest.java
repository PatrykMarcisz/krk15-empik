package pl.academy.sda.container;

import pl.academy.sda.dto.products.Product;
import pl.academy.sda.dto.products.game.Game;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReceiptGeneratorTest {

    @Mock
    private Cart cart;

    @Test
    @DisplayName("should set discount for cheapest Game when 3 at cart for same publisher")
    void gameDiscountTest() {
        //given
        Product GAME_1 = new Game(1, "firstGame", new BigDecimal(10), "xyz");
        Product GAME_2 = new Game(2, "secondGame", new BigDecimal(10), "xyz");
        Product GAME_3 = new Game(3, "thirdGame", new BigDecimal(9), "xyz");

        when(cart.getProductsInCart()).thenReturn(Arrays.asList(GAME_1, GAME_2, GAME_3));
        ReceiptGenerator receiptGenerator = new ReceiptGenerator(cart);

        List<String> recipe = receiptGenerator.createRecipe("USD");
        recipe.forEach(System.out::println);

        //when //fix it, shouldn't be in receipt generator
        BigDecimal priceAfterDiscount_1 = receiptGenerator.calculateDiscountForProduct(GAME_1);
        BigDecimal priceAfterDiscount_2 = receiptGenerator.calculateDiscountForProduct(GAME_2);
        BigDecimal priceAfterDiscount_3 = receiptGenerator.calculateDiscountForProduct(GAME_3);

        //then
        assertEquals(new BigDecimal(10), priceAfterDiscount_1);
        assertEquals(new BigDecimal(10), priceAfterDiscount_2);
        assertEquals(BigDecimal.ZERO, priceAfterDiscount_3);
    }

    @Test
    @DisplayName("should set discount for cheapest Game when 3 at cart for same publisher")
    void gameDiscountTest2() {
        //given
        Product GAME_1 = new Game(1, "firstGame", new BigDecimal(10), "xyz");
        Product GAME_2 = new Game(2, "secondGame", new BigDecimal(10), "xyz");
        Product GAME_3 = new Game(3, "thirdGame", new BigDecimal(9), "xyz");
        Product GAME_4 = new Game(4, "4", new BigDecimal(10), "xyz");
        Product GAME_5 = new Game(5, "5", new BigDecimal(10), "xyz");
        Product GAME_6 = new Game(6, "6", new BigDecimal(10), "xyz");
        Product GAME_7 = new Game(7, "7", new BigDecimal(10), "xyz");
        Product GAME_8 = new Game(8, "8", new BigDecimal(10), "xyz");
        Product GAME_9 = new Game(9, "9", new BigDecimal(5), "xyz");
        Product GAME_10 = new Game(10, "10", new BigDecimal(4), "xyz");

        when(cart.getProductsInCart()).thenReturn(Arrays.asList(GAME_1, GAME_2, GAME_3, GAME_4, GAME_5, GAME_6, GAME_7, GAME_8, GAME_9, GAME_10));
        ReceiptGenerator receiptGenerator = new ReceiptGenerator(cart);

        //when
        BigDecimal priceAfterDiscount = receiptGenerator.calculateDiscountForProduct(GAME_1);
        BigDecimal priceAfterDiscount_2 = receiptGenerator.calculateDiscountForProduct(GAME_2);
        BigDecimal priceAfterDiscount_3 = receiptGenerator.calculateDiscountForProduct(GAME_3);
        BigDecimal priceAfterDiscount_4 = receiptGenerator.calculateDiscountForProduct(GAME_4);
        BigDecimal priceAfterDiscount_5 = receiptGenerator.calculateDiscountForProduct(GAME_5);
        BigDecimal priceAfterDiscount_6 = receiptGenerator.calculateDiscountForProduct(GAME_6);
        BigDecimal priceAfterDiscount_7 = receiptGenerator.calculateDiscountForProduct(GAME_7);
        BigDecimal priceAfterDiscount_8 = receiptGenerator.calculateDiscountForProduct(GAME_8);
        BigDecimal priceAfterDiscount_9 = receiptGenerator.calculateDiscountForProduct(GAME_9);
        BigDecimal priceAfterDiscount_10 = receiptGenerator.calculateDiscountForProduct(GAME_10);

        //then
        assertEquals(new BigDecimal(10), priceAfterDiscount);
        assertEquals(new BigDecimal(10), priceAfterDiscount_2);
        assertEquals(new BigDecimal(10), priceAfterDiscount_4);
        assertEquals(new BigDecimal(10), priceAfterDiscount_5);
        assertEquals(new BigDecimal(10), priceAfterDiscount_6);
        assertEquals(new BigDecimal(10), priceAfterDiscount_7);
        assertEquals(new BigDecimal(10), priceAfterDiscount_8);
        assertEquals(BigDecimal.ZERO, priceAfterDiscount_3);
        assertEquals(BigDecimal.ZERO, priceAfterDiscount_9);
        assertEquals(BigDecimal.ZERO, priceAfterDiscount_10);
    }
}